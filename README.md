United Ideas Dictionary
The repository presents very simple dictionary with expressions and meaning.
Sample project is here: https://uidictionary.herokuapp.com/

Contents
  Database: Postgres
  Configuration YAML: /config/database.yml
  Polish rails Internationalization (I18n):
    Folder:
      /config/locales
        Files:
          /devise.pl.yml
          /pl.yml
          /simple_form.pl.yml

How to run
  I. Console:
    I.I bundle install
    I.II rake db:setup
    I.III rake db:migrate
  II. Browser
    II.I Run http://localhost:3000/
