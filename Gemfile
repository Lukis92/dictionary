ruby '2.2.3'
source 'https://rubygems.org'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.2.5'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .js.coffee assets and views
gem 'coffee-rails', '~> 4.1.0'
# Use jquery as the JavaScript library
gem 'jquery-rails'
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'

group :doc do
  # bundle exec rake doc:rails generates the API under doc/api.
  gem 'sdoc', '~> 0.4.0', require: false
end

group :development, :test do
  gem 'byebug'
end

group :development do
  gem 'web-console', '~> 2.0'
  gem 'spring'
end

group :production do
  gem 'rails_12factor'
end

gem 'bootstrap-sass' #Official Sass port of Bootstrap 2 and 3. http://getbootstrap.com/css/#sass
gem "haml-rails" # HAML templating.
gem 'devise' # Easy authorization library.
gem 'pg' # Use postgresql as the database for Active Record
gem 'tinymce-rails' #Integration of TinyMCE with the Rails asset pipeline
gem 'tinymce-rails-langs' #Language packs for tinymce-rails
gem 'html_truncator' #gem to truncate words without html tags
gem 'will_paginate' #Pagination library for Rails
gem 'bootstrap-will_paginate' #Format will_paginate html to match Twitter Bootstrap styling
gem "jquery-ui-rails" #jQuery UI for the Rails asset pipeline
gem 'rails4-autocomplete' #Use jQuery's autocomplete plugin with Rails 4
gem 'simple_form' # REALLY Simple Form builder.
gem 'paper_trail' #Track changes to your models' data. Good for auditing or versioning.
gem 'annotate' #Annotate Rails classes with schema and routes info
gem 'pg_search' # Easy finders for Rails.
gem 'better_errors' # Better error page for Rails and other Rack apps.
gem 'foreman' #Manage Procfile-based applications http://ddollar.github.com/foreman
gem 'puma' #A ruby web server built for concurrency http://puma.io
