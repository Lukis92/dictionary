json.phrases @phrases do |phrase|
  json.expression phrase.expression
  json.meaning phrase.meaning
  #get url images
  scrubber = Rails::Html::PermitScrubber.new
  scrubber.tags = ['img']
  scrubber.attributes = %w(src)
  html_fragment = Loofah.fragment(phrase.meaning)
  html_fragment.scrub!(scrubber)
  json.meaning html_fragment.to_s
end
