# == Schema Information
#
# Table name: tests
#
#  id         :integer          not null, primary key
#  content    :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Test < ActiveRecord::Base
  validates :content, presence: true
end
