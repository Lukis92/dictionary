# == Schema Information
#
# Table name: phrases
#
#  id         :integer          not null, primary key
#  expression :string
#  meaning    :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Phrase < ActiveRecord::Base
  include PgSearch
  PgSearch.multisearch_options = { :using => { :tsearch => {:prefix => true} } }
  multisearchable :against => [:expression, :meaning]
  has_paper_trail :on => [:update]
  belongs_to :user
  validates :expression, :meaning, presence: true
end
