# == Schema Information
#
# Table name: tests
#
#  id         :integer          not null, primary key
#  content    :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class TestsController < ApplicationController
  before_action :set_test, only: [:edit,:update]
  before_action :check_user, only: [:update]

  def index
    @tests = Test.all
  end

  def update
      if @test.update(test_params)
        redirect_to tests_path, notice: 'Pomyślnie zaktualizowano.'
      end
  end

  # def show
  #   @test = Test.find(params[:id])
  # end
  private
    def set_test
      @test = Test.find(params[:id])
    end

    def test_params
      params.require(:test).permit(:content)
    end

    def check_user
      unless (current_user && current_user.admin?)
        redirect_to phrases_path, alert: "Nie masz uprawnień admina!"
      end
    end
end
