# == Schema Information
#
# Table name: phrases
#
#  id         :integer          not null, primary key
#  expression :string
#  meaning    :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class PhrasesController < ApplicationController
  before_action :set_phrase, only: [:show, :edit, :update, :destroy, :history]
  before_action :check_user, only: [:edit, :update, :destroy]
  autocomplete :phrase, :expression

  def search
    if params[:search].present?
      @phrases = PgSearch.multisearch(params[:search]).paginate(:page => params[:page], :per_page => 4)
    end
  end
  # GET /phrases
  # GET /phrases.json
  def index
    respond_to do |format|
      format.html { @phrases = Phrase.paginate(page: params[:page], per_page: 4) }
      format.json { @phrases = Phrase.all }
    end
  end

  # GET /phrases/new
  def new
    @phrase = Phrase.new
  end

  # POST /phrases
  # POST /phrases.json
  def create
    @phrase = Phrase.new(phrase_params)

    respond_to do |format|
      if @phrase.save
        format.html { redirect_to @phrase, notice: 'Pomyślnie dodano.' }
        format.json { render :show, status: :created, location: @phrase }
      else
        format.html { render :new }
        format.json { render json: @phrase.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /phrases/1
  # PATCH/PUT /phrases/1.json
  def update
    respond_to do |format|
      if @phrase.update(phrase_params)
        format.html { redirect_to @phrase, notice: 'Pomyślnie zaktualizowano.' }
        format.json { render :show, status: :ok, location: @phrase }
      else
        format.html { render :edit }
        format.json { render json: @phrase.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /phrases/1
  # DELETE /phrases/1.json
  def destroy
    @phrase.destroy
    respond_to do |format|
      format.html { redirect_to phrases_url, notice: 'Pomyślnie usunięto.' }
      format.json { head :no_content }
    end
  end

  def history
    #unscoped - returns a scope for the model without the previously set scopes
    @versions = @phrase.versions.merge(PaperTrail::Version.unscope(:order)).order('created_at DESC').paginate(:page => params[:page], :per_page => 5)
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_phrase
      @phrase = Phrase.find(params[:id])
    end
    # Never trust parameters from the scary internet, only allow the white list through.
    def phrase_params
      params.require(:phrase).permit(:expression, :meaning)
    end

    def check_user
      unless (current_user && current_user.admin?)
        redirect_to phrases_path, alert: "Nie masz uprawnień admina!"
    end
end
end
