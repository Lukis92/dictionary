class CreatePhrases < ActiveRecord::Migration
  def change
    create_table :phrases do |t|
      t.string :expression
      t.string :meaning

      t.timestamps null: false
    end
  end
end
