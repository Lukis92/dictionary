Rails.application.routes.draw do
  resources :phrases do
    collection do
      get 'search'
      get 'home' => 'home#index'
    end
    member do
      get 'history'
    end
    get :autocomplete_phrase_expression, :on => :collection
  end
  resources :tests do
    collection do
      get 'tests' => 'tests#index'
      get 'edit' => 'tests#edit'
    end
  end
  devise_for :users
   root 'home#index'
end
